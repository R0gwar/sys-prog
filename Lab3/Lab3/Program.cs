﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3
{
    class Program
    {
        static double Function(double x)
        {
            return Math.Sin(2 * x - 125 * x * x * x + Math.Sqrt(2 * x) - 1.3 * 8 * x * x * x);
        }
        static void Main(string[] args)
        {

        StartOfCalculations:
            Console.Write("Введiть початок вiдрiзку iнтегрування a:");
            string sa = Console.ReadLine();
            double a = double.Parse(sa);

            Console.Write("Введiть кiнець вiдрiзку iнтегрування b: ");
            string sb = Console.ReadLine();
            double b = double.Parse(sb);

            Console.WriteLine("Введiть кiлькiсть дiлянок n:");
            string sn = Console.ReadLine();
            double n = double.Parse(sn);

            double dx = (b - a) / n;//длина каждого отрезка
            double xln, xln2;
            double y1 = 0, y2 = 0;
            //double Intgrl = 0;
            for (int i = 0; i < n; i++)
            {
                a += dx;
                xln = a + dx;
                xln2 = Function(xln);
                if (i > 0)
                {
                    y1 += xln2;
                }
            }
            y2 = dx * y1;

            Console.WriteLine("Iнтеграл функцiї на вiдрiзку [{0}, {1}] становить {2:0.00000}", a, b, y2);
            Console.Write("Повторити розрахунок (y - так) ? ");
            ConsoleKeyInfo pressedKey = Console.ReadKey(); Console.WriteLine();
            if (pressedKey.Key == ConsoleKey.Y)
            {
                Console.WriteLine();
                goto StartOfCalculations;
            }
        }
    }
}
