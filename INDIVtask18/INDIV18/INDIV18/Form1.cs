﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace INDIV18
{
    public partial class Form1 : Form
    {
        Bitmap image;
        bool accIsCreated = false;
        public Form1()
        {
            InitializeComponent();
            nameField.Validating += nameField_Validating;
            logField.Validating += logField_Validating;
            ageField.Validating += ageField_Validating;
            passField.Validating += passField_Validating;
            logField.Validated += textBox1_TextChanged;
            openFileDialog1.Filter = "Image files (*.jpg)|*.jpg";
            saveFileDialog1.Filter = "Text files(*.txt)|*.txt|All files(*.*)|*.*";
            
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (logField.Text.Length > 0)
            {
                createButton.Enabled = true;
            }
        }
        private void createButton_Click(object sender, EventArgs e)
        {
            int age;
            string userLogin, userName, userPassword;
            if (!int.TryParse(ageField.Text, out age) || String.IsNullOrEmpty(logField.Text) || String.IsNullOrEmpty(passField.Text) || String.IsNullOrEmpty(nameField.Text) || checkBox1.Checked == false)
            {
                MessageBox.Show("Заполните поля и поставьте галочку!");
            }
            else
            {
               
                age = int.Parse(ageField.Text);
                userLogin = logField.Text;
                userPassword = passField.Text;
                userName = nameField.Text;
                string ageString = age.ToString();
                string accInfo = (" user name: " + userName + "\n Login: " + userLogin + "\n Password: " + userPassword + "\n Age: " + age);

                //TESTFIELDS
                /* textBox1.Text = age.ToString();
                textBox2.Text = userLogin.ToString();
                textBox3.Text = userPassword.ToString();
                textBox4.Text = userName.ToString();
                */

                if (saveFileDialog1.ShowDialog() == DialogResult.Cancel)
                    return;
               
                string account = saveFileDialog1.FileName;
                
                System.IO.File.WriteAllText(account, accInfo);
                MessageBox.Show("Аккаунт успешно создан на вашем диске");
                accIsCreated = true;
                if (picBox.Image != null) 
                { 
                    SaveFileDialog savedialog = new SaveFileDialog();
                    savedialog.Title = "Сохранить картинку как...";
                    savedialog.OverwritePrompt = true;
                    savedialog.CheckPathExists = true;
                    savedialog.Filter = "Image Files(*.BMP)|*.BMP|Image Files(*.JPG)|*.JPG|Image Files(*.GIF)|*.GIF|Image Files(*.PNG)|*.PNG|All files (*.*)|*.*";  
                    savedialog.ShowHelp = true;
                    if (savedialog.ShowDialog() == DialogResult.OK) 
                    {
                        try
                        {
                            image.Save(savedialog.FileName, System.Drawing.Imaging.ImageFormat.Jpeg);
                            MessageBox.Show("Фото добавлено к вашему аккаунту");
                        }
                        catch
                        {
                            MessageBox.Show("Невозможно сохранить изображение", "Ошибка",
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                }
            }
            
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
        private void nameField_Validating(object sender, CancelEventArgs e)
        {
            if (String.IsNullOrEmpty(nameField.Text))
            {
                errorProvider1.SetError(nameField, "Не указано имя!");
            }
            else if (nameField.Text.Length < 2)
            {
                errorProvider1.SetError(nameField, "Слишком короткое имя!");
            }
            else
            {
                errorProvider1.Clear();
            }
        }
        private void logField_Validating(object sender, CancelEventArgs e)
        {
            if (String.IsNullOrEmpty(logField.Text))
            {
                errorProvider1.SetError(logField, "Не указан логин!");
            }
            else if (logField.Text.Length < 4)
            {
                errorProvider1.SetError(logField, "Слишком короткий логин!");
            }
            else
            {
                errorProvider1.Clear();
            }
        }
        private void ageField_Validating(object sender, CancelEventArgs e)
        {
            double age = 0;
            if (String.IsNullOrEmpty(ageField.Text))
            {
                errorProvider1.SetError(ageField, "Не указан возраст!");
            }
            else if (!Double.TryParse(ageField.Text, out age))
            {
                errorProvider1.SetError(ageField, "Некорретный возраст!");
            } 
            else
            {
                errorProvider1.Clear();
            }

        }
        private void passField_Validating(object sender, CancelEventArgs e)
        { 
            if (String.IsNullOrEmpty(passField.Text))
            {
                errorProvider1.SetError(passField, "Enter password");
            }
            else
            {
                errorProvider1.Clear();
            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            
            if (checkBox1.Checked == false)
            {
                errorProvider1.SetError(checkBox1, "Accept!");
            }
            else
            {
                errorProvider1.Clear();
            }
        }

       

        private void picBox_Click(object sender, EventArgs e)
        {
            
            OpenFileDialog open_dialog = new OpenFileDialog(); 
            open_dialog.Filter = "Image Files(*.BMP;*.JPG;*.GIF;*.PNG)|*.BMP;*.JPG;*.GIF;*.PNG|All files (*.*)|*.*"; 
            if (open_dialog.ShowDialog() == DialogResult.OK) 
            {
                try
                {
                    image = new Bitmap(open_dialog.FileName);
                    this.picBox.Size = image.Size;
                    picBox.Image = image;
                    picBox.Invalidate();
                }
                catch
                {
                    DialogResult rezult = MessageBox.Show("Невозможно открыть выбранный файл",
                    "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        private void phButton_Click(object sender, EventArgs e)
        {
            //на всякий случай добавил возможность добавить фото уже после создания аккаунта
            if (accIsCreated == false)
            {
                errorProvider1.SetError(phButton, "Сначала создайте аккаунт");
            }
            else
            {
                
                if (picBox.Image != null)
                {
                    errorProvider1.Clear();
                    SaveFileDialog savedialog = new SaveFileDialog();
                    savedialog.Title = "Сохранить картинку как...";
                    savedialog.OverwritePrompt = true;
                    savedialog.CheckPathExists = true;
                    savedialog.Filter = "Image Files(*.BMP)|*.BMP|Image Files(*.JPG)|*.JPG|Image Files(*.GIF)|*.GIF|Image Files(*.PNG)|*.PNG|All files (*.*)|*.*";
                    savedialog.ShowHelp = true;
                    if (savedialog.ShowDialog() == DialogResult.OK)
                    {
                        try
                        {
                            image.Save(savedialog.FileName, System.Drawing.Imaging.ImageFormat.Jpeg);
                            MessageBox.Show("Фото добавлено к вашему аккаунту");
                        }
                        catch
                        {
                            MessageBox.Show("Невозможно сохранить изображение", "Ошибка",
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                }
            }
        }
    }
}