﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LAb4
{
    class Program
    {
        const double StartX = 10.3;
        const double dx = 0.7;
        static double Function(double x)
        {
            //x1=2.76x x2=0.5x
            return Math.Cos(Math.Sqrt(0.5 * x) + 93.84 * x) - 4 * Math.Sin(0.5 * x);
        }
        static void Main(string[] args)
        {
            double[] arr = new double[10];
            double x = StartX;
            for (int i = arr.GetLowerBound(0); i <= arr.GetUpperBound(0); i++)
            {
                arr[i] = Function(x);
                x += dx;
            }
            Array.Sort(arr);
            Array.Reverse(arr);
            Console.WriteLine("Вiдсортованi за спаданням значення масиву: ");
            for (int i = arr.GetLowerBound(0); i <= arr.GetUpperBound(0); i++)
            {
                Console.WriteLine("arr[{0:00}] = {1:0.0000}", i, arr[i]);

            }
            double aMin = arr[arr.GetUpperBound(0)];
            double aMax = arr[arr.GetLowerBound(0)];
            double aAvg = 0;
            int count = 0;
            double aMin10 = aMin + 0.01 * aMin;
            for (int i = arr.GetLowerBound(0); i <= arr.GetUpperBound(0); i++)
            {
                aAvg += arr[i];
                if (arr[i] >= aMin && arr[i] <= aMin10)
                {
                    count++;
                }
            }
            aAvg = aAvg / arr.GetLength(0);
            Console.WriteLine("Мiнiмальне значення масиву: {0:0.0000}", aMin);
            Console.WriteLine("Максимальне значення масиву: {0:0.0000}", aMax);
            Console.WriteLine("Середнє значення масиву: {0:0.0000}", aAvg);
            Console.WriteLine("Кількість елементів у межах (R): {0:0.0000}", count);
            Console.ReadKey(true);
        }
    }
}