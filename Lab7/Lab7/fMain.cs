﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab7
{
    public partial class fMain : Form
    {
        public fMain()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void btnCalculate_Click(object sender, EventArgs e)
        {
            if(string.IsNullOrEmpty(tbX1.Text) || (string.IsNullOrEmpty(tbX2.Text)))
            {
                tBy.Text = "kavo";
                return;
            }
            double x1 = double.Parse(tbX1.Text);
            double x2 = double.Parse(tbX2.Text);
            double y = Math.Pow(Math.Cos(x1 - Math.Sqrt(x2/(x1-53*x2*x2))) ,4);
            tBy.Text = y.ToString("0.###");
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            tbX1.Text = string.Empty;
            tbX2.Text = string.Empty;
            tBy.Text = string.Empty;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void bLs_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(tbX1.Text) || (string.IsNullOrEmpty(tbX2.Text)))
            {
                tBy.Text = "kavo";
                return;
            }
            double x1 = double.Parse(tbX1.Text);
            double x2 = double.Parse(tbX2.Text);
            if(x1 < x2)
            {
                tBy.Text = x1.ToString();
            }
            else
            {
                tBy.Text = x2.ToString();
            }
        }
    }
}
