﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LAb5
{
    class Program
    {
        class Person
        {
            public string name;
            public int age;
            public string nation;
            public int height;
            public string gender;
            public string occupation;
            public bool criminal_Exp;
            public int weight;
            public double fat_Percentage()
            {
                return height / weight;
            }
        }
        static void Main(string[] args)
        {
            Console.Write("Enter Name: ");
            string sName = Console.ReadLine();

            Console.Write("Enter Age: ");
            string sAge = Console.ReadLine();

            Console.Write("Enter Country: ");
            string sNation = Console.ReadLine();

            Console.Write("Enter Height: ");
            string sHeight = Console.ReadLine();

            Console.Write("Enter Gender: ");
            string sGender = Console.ReadLine();

            Console.Write("Enter Ocupation: ");
            string sOccupation = Console.ReadLine();

            Console.Write("Was arrested? (y-так, n-нi): ");
            ConsoleKeyInfo keyArrested = Console.ReadKey();
            Console.WriteLine();

            Console.Write("Enter Weight: ");
            string sWeight = Console.ReadLine();

            Person myPerson = new Person();

            myPerson.name = sName;
            myPerson.age = int.Parse(sAge);
            myPerson.nation = sNation;
            myPerson.height = int.Parse(sHeight);
            myPerson.gender = sGender;
            myPerson.occupation = sOccupation;
            myPerson.weight = int.Parse(sWeight);
            myPerson.criminal_Exp = keyArrested.Key == ConsoleKey.Y ? true : false;

            double fat_Percantege = myPerson.fat_Percentage();
              Console.WriteLine();
              Console.WriteLine("------------------------------------------------");
              Console.WriteLine("Данi про людину: ");
              Console.WriteLine("------------------------------------------------");
              Console.WriteLine("Ім'я: " + myPerson.name);
              Console.WriteLine("Вік: " + myPerson.age.ToString());
              Console.WriteLine("Національність: " + myPerson.nation);
              Console.WriteLine("Зріст: " + myPerson.height.ToString());
              Console.WriteLine("Стать: " + myPerson.gender);
              Console.WriteLine("Місце праці: " + myPerson.occupation);
              Console.WriteLine(myPerson.criminal_Exp ? "Засуджений був" : "Засуджений не був");
              Console.WriteLine();
            Console.WriteLine("Вага: " + myPerson.weight.ToString("0.000"));
            Console.WriteLine("Процент жиру: " + fat_Percantege.ToString("0.00"));
              Console.ReadKey(); 
        }
    }
}