﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Введiть початкове значення X1min:");
            string sx1Min = Console.ReadLine();
            double x1Min = Double.Parse(sx1Min);
            Console.Write("Введiть кiнцеве значення X1max:");
            string sx1Max = Console.ReadLine();
            double x1Max = double.Parse(sx1Max);
            Console.Write("Введiть прирiст dX1: ");
            string sdx1 = Console.ReadLine();
            double dx1 = double.Parse(sdx1);
            Console.Write("Введiть початкове значення X2min: ");
            string sx2Min = Console.ReadLine();
            double x2Min = Double.Parse(sx2Min);
            Console.Write("Введiть кiнцеве значення X2max: ");
            string sx2Max = Console.ReadLine();
            double x2Max = double.Parse(sx2Max);
            Console.Write("Введiть прирiст dX2: ");
            string sdx2 = Console.ReadLine();
            double dx2 = double.Parse(sdx2);
            double ytr = 0;
            double S = 0;

            for (double x2 = x2Min; x2 <= x2Max; x2 += dx2)
            {

                Console.Write("{0: 0.00}\t", x2);
                for (double x1 = x1Min; x1 <= x1Max; x1 += dx1)
                {
                    double y = 45 * x1 * Math.Sin(x2) + 3 * x1 * Math.Sqrt(x1) * Math.Sqrt(x2);
                    Console.Write("{0:0.000}\t", y);
                    if (y > 0)
                    {

                        ytr = y * y * y;
                        S = +ytr;
                    }

                }
                Console.WriteLine();
            }
            Console.WriteLine("Tr sum of y TR = {0:#.###}\t", S);
            Console.ReadKey();
        }
    }
}