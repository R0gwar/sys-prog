﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab6;

namespace Lab06app
{
    class Program
    {
        static void Main(string[] args)
        {
            Person[] arrPeople;
            Console.Write("Введiть кiлькiсть мiст: ");
            int cntPeople = int.Parse(Console.ReadLine());
            arrPeople = new Person[cntPeople];

            for (int i = 0; i < cntPeople; i++)
            {
                Console.Write("Enter Name: ");
                string sName = Console.ReadLine();

                Console.Write("Enter Age: ");
                string sAge = Console.ReadLine();

                Console.Write("Enter Country: ");
                string sNation = Console.ReadLine();

                Console.Write("Enter Height: ");
                string sHeight = Console.ReadLine();

                Console.Write("Enter Gender: ");
                string sGender = Console.ReadLine();

                Console.Write("Enter Ocupation: ");
                string sOccupation = Console.ReadLine();

                Console.Write("Was arrested? (y-так, n-нi): ");
                ConsoleKeyInfo keyArrested = Console.ReadKey();
                Console.WriteLine();

                Console.Write("Enter Weight: ");
                string sWeight = Console.ReadLine();

                Person myPerson = new Person();

                myPerson.name = sName;
                myPerson.age = int.Parse(sAge);
                myPerson.nation = sNation;
                myPerson.height = int.Parse(sHeight);
                myPerson.gender = sGender;
                myPerson.occupation = sOccupation;
                myPerson.weight = int.Parse(sWeight);
                myPerson.criminal_Exp = keyArrested.Key == ConsoleKey.Y ? true : false;

                double fat_Percent = myPerson.Getfat_Percent();
                arrPeople[i] = myPerson;
            }
            foreach (Person t in arrPeople)
            {
                Console.WriteLine();
                Console.WriteLine("------------------------------------------------");
                Console.WriteLine("Данi про людину: ");
                Console.WriteLine("------------------------------------------------");
                Console.WriteLine("Ім'я: " + t.name);
                Console.WriteLine("Вік: " + t.age.ToString());
                Console.WriteLine("Національність: " + t.nation);
                Console.WriteLine("Зріст: " + t.height.ToString());
                Console.WriteLine("Стать: " + t.gender);
                Console.WriteLine("Місце праці: " + t.occupation);
                Console.WriteLine(t.criminal_Exp ? "Засуджений був" : "Засуджений не був");
                Console.WriteLine();
                Console.WriteLine("Вага: " + t.weight.ToString("0.000"));
                Console.WriteLine("Процент жиру: " + t.fat_Percent.ToString("0.00"));
                Console.ReadKey();
            }
        }

    }
}
