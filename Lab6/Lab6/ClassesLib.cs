﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab6
{
    public class Person
    {
        public string name;
        public int age;
        public string nation;
        public int height;
        public string gender;
        public string occupation;
        public bool criminal_Exp;
        public int weight;
        public double fat_Percent
        {
            get
            {
              return  Getfat_Percent();
            }
        }
        public double Getfat_Percent()
        {
            return height / weight;
        }
        public string religion;
    }

}
